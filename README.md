**Assignment1_Prog8560**

This is Assignment_1 for class Prog 8560 to familiarize with bitbucket

**Getting Started**

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

1- create directory for where you would like to clone this repository locally
2- git clone via https or ssh so that repository is now present locally in machine
3- run html file with Live server to render index.html 


**License & copyright**

Licensed under the [MIT License](LICENSE) - the reason why MIT LIcense was picked is because it is one of the most permissive and popular licenses.  It offers low protection for the author of the software and source code doesn't need to be public when distribution of the software is made.  Modifications can be released under any license and it offers no explicit postion on patent usage.

MIT License is very permissive and welcoming to open source developers.

References:

"https://www.freecodecamp.org/news/how-open-source-licenses-work-and-how-to-add-them-to-your-projects-34310c3cf94/"